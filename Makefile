# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/05/29 15:51:50 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = push_swap

SRC_PATH = ./src/
SRC_NAME = ft_push.c ft_push_lib1.c ft_push_lib2.c ft_push_lib3.c ft_push_lib4.c\
			ft_reverse_rotate.c ft_swap.c ft_rotate.c main.c ft_bubble_swap.c\
			ft_merge_swap.c ft_quick_swap.c ft_push_preload.c ft_printstate.c\
			ft_gnome_swap.c ft_insertion_swap.c ft_check_error.c\
			ft_this_sort.c ft_order.c ft_write_name.c quick.c ft_cocktail.c\
			inser.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./inc/

CFLAGS = -Wall -Werror -Wextra -g

LIB_PATHS = ./printf/

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(addprefix -L,$(LIB_PATHS))

LIB_NAME = -lftprintf
LDFLAGS = $(LIB) $(LIB_NAME)

all: $(NAME)

$(NAME): $(OBJ)
	$(foreach PATHS,$(LIB_PATHS),make -C $(PATHS);)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	rm -rf $(OBJ_PATH)

fclean: clean
	$(foreach PATHS,$(LIB_PATHS),make -C $(PATHS) fclean;)
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
