/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 06:04:02 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 11:15:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_H
# define PUSH_H

# include <unistd.h>
# include <stdlib.h>
# include <time.h>
# include "ft_stdio.h"

# define DEBUG 1

typedef struct		s_swap
{
	struct s_swap	*next;
	int				nbr;
}					t_swap;

typedef struct s_stack
{
	struct s_stack	*next;
	int				asc;
	int				nbr;
}				t_stack;

typedef enum		e_sort
{
	NSORT,
	ASORT,
	DSORT,
	UNDEF,
}					t_sort;

typedef struct		s_swap_d
{
	t_swap			*swap;
	size_t			len;
	int				sort;
	int				exept;
}					t_swap_d;

typedef struct		s_order
{
	int			asc;
	int			dsc;
	int			nb_last;
	char		first;
	char		last;
	char		l_last;
}					t_order;

typedef struct		s_opt
{
	int			base;
	int			(*algo)();
	int			print_time;
	int			print_score;
	int			print_state;
	int			print_last;
	int			silence;
	t_order		order;
	t_sort		sort[1];
}					t_opt;

typedef struct		s_assoc
{
	char			*name;
	int				(*algo)();
}					t_assoc;

int					ft_bubble_swap(t_swap **a, size_t len);
int					ft_cocktail(t_swap **a, size_t len);
int					ft_merge_swap(t_swap **a, size_t len);
int					ft_quick_swap(t_swap **a, size_t len);
int					ft_gnome_swap(t_swap **a, size_t len);
int					ft_insertion_swap(t_swap **a, size_t len);
int					quick(t_swap **a, size_t len);
int					sort_insert(t_swap **a, size_t len);

int					ft_this_swap(t_swap **a, t_order *ord, size_t len);

t_swap_d			*ft_preload(char **av, t_opt *o);
size_t				ft_strlen(const char *str);
int					ft_atoi(char *str);
int					ft_strcmp(char *s1, char *s2);
int					ft_strequ(char *s1, char *s2);
void				ft_bzero(void *mem, size_t size);
void				*ft_memalloc(size_t size);
void				ft_order(int nb, t_opt *o);

int					ft_check_error(char *nbr, t_swap_d *d);
void				ft_putchar(char c);
void				ft_putstr(char *str);
void				ft_putnbr_base(int nbr, int base);
void				ft_error(void);
void				ft_write_name(char *name, size_t size);

t_swap				*ft_swapnew(int nbr);
t_swap				*ft_swapadd(t_swap **head, t_swap *node);
void				ft_swapprint(t_swap *s, int base);
void				ft_printstate(t_swap *a, t_swap *b);

int					ft_sa(t_swap **a, t_swap **b);
int					ft_sb(t_swap **a, t_swap **b);
int					ft_ss(t_swap **a, t_swap **b);
int					ft_pa(t_swap **a, t_swap **b);
int					ft_pb(t_swap **a, t_swap **b);
int					ft_ra(t_swap **a, t_swap **b);
int					ft_rb(t_swap **a, t_swap **b);
int					ft_rr(t_swap **a, t_swap **b);
int					ft_rra(t_swap **a, t_swap **b);
int					ft_rrb(t_swap **a, t_swap **b);
int					ft_rrr(t_swap **a, t_swap **b);

#endif
