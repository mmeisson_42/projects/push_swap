/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:13 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:39:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

extern t_opt	g_o;

int			ft_ra(t_swap **a, t_swap **b)
{
	t_swap		*tmp;
	t_swap		*prev;

	if (b)
		b = NULL;
	prev = NULL;
	tmp = *a;
	while (tmp)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if (prev)
	{
		prev->next = *a;
		*a = (*a)->next;
		prev->next->next = NULL;
		if (g_o.silence == 0)
			ft_write_name("ra", 2);
		return (1);
	}
	return (0);
}

int			ft_rb(t_swap **a, t_swap **b)
{
	t_swap		*tmp;
	t_swap		*prev;

	if (a)
		a = NULL;
	prev = NULL;
	tmp = *b;
	while (tmp)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if (prev)
	{
		prev->next = *b;
		*b = (*b)->next;
		prev->next->next = NULL;
		if (g_o.silence == 0)
			ft_write_name("rb", 2);
		return (1);
	}
	return (0);
}

int			ft_rr(t_swap **a, t_swap **b)
{
	int		ret;

	ret = 0;
	ret = ft_ra(a, b);
	ret += ft_rb(a, b);
	if (ret > 0 && g_o.silence)
		ft_write_name("rr", 2);
	return (ret) ? 1 : 0;
}
