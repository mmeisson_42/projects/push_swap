/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bubble_swap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:04:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:29:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

/*
** Bubble swap algorithm
** Efficacity :
** `len` operations on best case (In already sorted list)
** `len² + len` operations on worst case (reversed list)
*/

int		ft_bubble_swap(t_swap **a, size_t len)
{
	t_swap	*b;
	int		tem;
	size_t	i;
	int		score;

	b = NULL;
	score = 0;
	tem = 1;
	while (tem == 1)
	{
		tem = 0;
		i = 0;
		while (i++ < len - 1)
		{
			if ((*a)->nbr > (*a)->next->nbr)
			{
				tem = 1;
				score += ft_sa(a, &b);
			}
			score += ft_ra(a, &b);
		}
		score += ft_ra(a, &b);
		ft_printstate(*a, b);
	}
	return (score);
}
