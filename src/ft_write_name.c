/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_write_name.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 06:20:52 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:42:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

void		ft_write_name(char *name, size_t size)
{
	static int		first = 1;

	if (first == 0)
		write(1, " ", 1);
	write(1, name, size);
	first = 0;
}
