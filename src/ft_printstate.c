/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printstate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 17:58:46 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/21 18:33:43 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

extern t_opt		g_o;

void	ft_printstate(t_swap *a, t_swap *b)
{
	if (g_o.print_state == 1)
	{
		write(1, "\na : ", 5);
		ft_swapprint(a, g_o.base);
		write(1, "b : ", 4);
		ft_swapprint(b, g_o.base);
	}
}
