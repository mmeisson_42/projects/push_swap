/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_lib4.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:31 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:15:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

t_swap		*ft_swapnew(int nbr)
{
	t_swap		*node;

	node = ft_memalloc(sizeof(t_swap));
	node->nbr = nbr;
	return (node);
}

t_swap		*ft_swapadd(t_swap **head, t_swap *node)
{
	t_swap		*tmp;

	if (!*head)
	{
		*head = node;
		return (node);
	}
	else
	{
		tmp = *head;
		while (tmp->next)
		{
//			if (tmp->nbr == node->nbr)
//				ft_error();
			tmp = tmp->next;
		}
		tmp->next = node;
		return (node);
	}
}

void		ft_swapprint(t_swap *s, int base)
{
	while (s)
	{
		ft_putnbr_base(s->nbr, base);
		if (s->next)
			ft_putchar(' ');
		s = s->next;
	}
	ft_putchar('\n');
}
