/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_this_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 03:18:37 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 15:30:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static int	ft_swap_last(t_swap **a, t_swap *b)
{
	ft_rra(a, &b);
	ft_rra(a, &b);
	ft_sa(a, &b);
	ft_ra(a, &b);
	ft_ra(a, &b);
	return (5);
}

int			ft_else_swap(t_swap **a, t_order *ord, size_t len)
{
	t_swap	*b;

	b = NULL;
	if (ord->dsc == 0)
		return (0);
	else if (ord->dsc == 1 && ord->last == '+' && (*a)->nbr > ord->nb_last)
		return (ft_rra(a, &b));
	else if (ord->dsc == 1 && ord->first == '+' && (*a)->nbr > ord->nb_last)
		return (ft_ra(a, &b));
	else if (ord->dsc == 1 && ord->first == '+')
	{
		if ((*a)->nbr < (*a)->next->next->nbr)
			return (ft_sa(a, &b));
		else
			return (ft_bubble_swap(a, len));
	}
	else if (ord->dsc == 1 && ord->last == '+' && ord->l_last == '-')
		return (ft_swap_last(a, b));
	else if (ord->dsc * 30 <= ord->asc || ord->dsc == 1)
		return (ft_bubble_swap(a, len));
	return (-1);
}

int			ft_this_three2(t_swap **a, t_swap **b)
{
	int		score;

	score = 0;
	if ((*a)->nbr > (*a)->next->next->nbr)
	{
		if ((*a)->next->nbr < (*a)->next->next->nbr)
			score += ft_ra(a, b);
		else
		{
			score += ft_ra(a, b);
			score += ft_sa(a, b);
		}
	}
	else
		score += ft_sa(a, b);
	return (score);
}

int			ft_this_three(t_swap **a, t_swap **b)
{
	int		score;

	score = 0;
	if ((*a)->nbr < (*a)->next->nbr)
	{
		if ((*a)->nbr > (*a)->next->next->nbr)
			score += ft_rra(a, b);
		else
		{
			score += ft_rra(a, b);
			score += ft_sa(a, b);
		}
	}
	else
	{
		return (ft_this_three2(a, b));
	}
	return (score);
}

int			ft_this_swap(t_swap **a, t_order *ord, size_t len)
{
	int		score;
	t_swap	*b;

	b = NULL;
	score = 0;
	if (len < 2)
		return (0);
	else if (len == 2)
	{
		if ((*a)->nbr > (*a)->next->nbr)
			return (ft_sa(a, &b));
		return (0);
	}
	else if (len == 3 && ord->dsc != 0)
		return (ft_this_three(a, &b));
	else
		return (ft_else_swap(a, ord, len));
}
