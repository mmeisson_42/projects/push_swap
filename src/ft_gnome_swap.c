/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnome_swap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 23:49:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 04:41:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

int			ft_gnome_swap(t_swap **a, size_t len)
{
	t_swap		*b;
	int			i;
	int			score;

	i = 0;
	score = 0;
	b = NULL;
	while (i < (int)len - 1)
	{
		while (i >= 0 && (*a)->nbr > (*a)->next->nbr)
		{
			score += ft_sa(a, &b);
			score += ft_rra(a, &b);
			i--;
		}
		score += ft_ra(a, &b);
		ft_printstate(*a, b);
		i++;
	}
	score += ft_ra(a, &b);
	return (score);
}
