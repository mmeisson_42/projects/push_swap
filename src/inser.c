#include "push.h"

int		sort_insert(t_swap **a, size_t size)
{
	t_swap        *b;
	size_t        i;
	int			score;

	i = 0;
	score = 0;
	b = NULL;
	while (i < size)
	{
		if ((*a)->nbr < (*a)->next->nbr)
		{
			score += ft_pb(a, &b);
			while (b->nbr < (*a)->nbr && i > 0)
			{
				score += ft_rra(a, &b);
				i--;
			}
			score += ft_ra(a, &b);
			score += ft_pa(a, &b);
			i++;
		}
		score += ft_ra(a, &b);
		i++;
	}
	while (size-- > 1)
		score += ft_rra(a, &b);
	return (score);
}
