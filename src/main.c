/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:05:02 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 15:49:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

t_opt		g_o;

static int		ft_default(t_swap_d *a)
{
	int		score;

	score = ft_this_swap(&(a->swap), &(g_o.order), a->len);
	if (score == -1)
		score = ft_merge_swap(&(a->swap), a->len);
	return (score);
}

int				main(int ac, char **av)
{
	t_swap_d	*a;
	int			score;
	clock_t		t;

	ft_bzero(&g_o, sizeof(t_opt));
	if (ac > 1)
	{
		a = ft_preload(av, &g_o);
		t = clock();
		if (g_o.algo)
			score = g_o.algo(&(a->swap), a->len, &g_o);
		else
			score = ft_default(a);
		if (!g_o.silence)
			write(1, "\n", 1);
		t = clock() - t;
		if (g_o.print_last)
			ft_swapprint(a->swap, g_o.base);
		if (g_o.print_time == 1)
			ft_printf("Time taken: %D ms\n", t / (CLOCKS_PER_SEC / 1000));
		if (g_o.print_score == 1)
			ft_printf("Score : %d\n", score);
	}
	return (0);
}
