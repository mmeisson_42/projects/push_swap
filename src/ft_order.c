/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_order.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/29 03:14:09 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 11:46:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static void		ft_cmp(int nb, int last, t_opt *o)
{
	if (nb > last)
		(o->order.asc)++;
	else
		(o->order.dsc)++;
}

void			ft_order(int nb, t_opt *o)
{
	static int			last;
	static int			l_last;
	static int			init = 0;

	if (init == 1)
	{
		o->order.first = (nb > last) ? '-' : '+';
		ft_cmp(nb, last, o);
	}
	else if (init > 1)
	{
		ft_cmp(nb, last, o);
		o->order.last = (nb > last) ? '-' : '+';
	}
	if (init > 2)
	{
		o->order.l_last = (nb > l_last) ? '-' : '+';
		l_last = last;
	}
	o->order.nb_last = nb;
	last = nb;
	init++;
}
