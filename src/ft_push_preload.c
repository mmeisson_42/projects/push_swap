/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_preload.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:52 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 09:18:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static void		ft_get_algo(char **arg, t_opt *o)
{
	const t_assoc		algo[] = {
		{ "bubble", ft_bubble_swap },
		{ "merge", ft_merge_swap },
		{ "quick", ft_quick_swap },
		{ "gnome", ft_gnome_swap },
		{ "insertion", ft_insertion_swap },
		{ "quicky", quick },
		{ "ins", sort_insert },
	};
	size_t				i;

	i = 0;
	while (i < sizeof(algo) / sizeof(*algo))
	{
		if (ft_strequ(algo[i].name, *arg))
		{
			o->algo = algo[i].algo;
			return ;
		}
		i++;
	}
	ft_error();
}

static void		ft_get_base(char **av, t_opt *o)
{
	char		*arg;

	arg = *av;
	if (*arg)
		while (*arg >= '0' && *arg <= '9')
			o->base = (o->base * 10) + (*(arg++) - 48);
	if (o->base < 2 || o->base > 16 || *arg)
		ft_error();
}

void			ft_default_options(t_opt *o)
{
	if (o->base == 0)
		o->base = 10;
}

void			ft_get_opts(char ***av, t_opt *o)
{
	char		**arg;

	arg = *av + 1;
	while (*arg && **arg == '-')
	{
		if (ft_strequ(*arg, "-t"))
			o->print_time = 1;
		else if (ft_strequ(*arg, "-sc"))
			o->print_score = 1;
		else if (ft_strequ(*arg, "-st"))
			o->print_state = 1;
		else if (ft_strequ(*arg, "-s"))
			o->silence = 1;
		else if (ft_strequ(*arg, "-d"))
			o->print_last = 1;
		else if (ft_strequ(*arg, "-b"))
			ft_get_base(arg++ + 1, o);
		else if (ft_strequ(*arg, "-a"))
			ft_get_algo(arg++ + 1, o);
		else
			break ;
		arg++;
	}
	ft_default_options(o);
	*av = arg;
}

t_swap_d		*ft_preload(char **av, t_opt *o)
{
	t_swap_d		*sw;
	t_swap			*new;

	sw = ft_memalloc(sizeof(t_swap_d));
	ft_get_opts(&av, o);
	while (*av)
	{
		new = ft_swapnew(ft_check_error(*av, sw));
		ft_order(new->nbr, o);
		ft_swapadd(&(sw->swap), new);
		sw->len++;
		av++;
	}
	return (sw);
}
