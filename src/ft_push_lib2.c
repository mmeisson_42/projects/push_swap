/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_lib2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/06/07 15:42:02 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

size_t		ft_strlen(const char *str)
{
	char	*tmp;

	tmp = (char *)str;
	while (*tmp)
		tmp++;
	return (tmp - str);
}

int			ft_atoi(char *str)
{
	unsigned int			ret;
	int						neg;

	ret = 0;
	while (*str == ' ' || *str == '\t')
		str++;
	neg = (*str == '-') ? 1 : 0;
	str += (*str == '-' || *str == '+') ? 1 : 0;
	while (*str)
	{
		if (*str < '0' || *str > '9')
			ft_error();
		ret = (ret * 10) + (*str - 48);
		str++;
	}
	return (neg) ? (-ret) : (ret);
}

void		ft_bzero(void *mem, size_t size)
{
	unsigned char		*m;

	m = (unsigned char*)mem;
	while (size--)
		m[size] = 0;
}

void		*ft_memalloc(size_t size)
{
	void	*ret;

	if (!(ret = malloc(size)))
		ft_error();
	ft_bzero(ret, size);
	return (ret);
}
