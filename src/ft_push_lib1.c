/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_lib1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:48 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:05:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

void		ft_putchar(char c)
{
	write(1, &c, 1);
}

void		ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void		ft_error(void)
{
	write(2, "Error\n", 6);
	exit(1);
}

void		ft_putnbr_base(int nbr, int base)
{
	const char		*str_base = "0123456789abcdef";
	unsigned int	n;
	unsigned int	b;

	if (base < 2 || base > 16)
		return ;
	b = (unsigned int)base;
	n = (nbr < 0) ? (unsigned int)-nbr : (unsigned int)nbr;
	if (nbr < 0)
		ft_putchar('-');
	if (n >= b)
		ft_putnbr_base(n / base, base);
	ft_putchar(str_base[n % base]);
}
