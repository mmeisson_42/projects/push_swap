/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:21 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:41:03 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

extern t_opt	g_o;

int		ft_pa(t_swap **a, t_swap **b)
{
	t_swap		*tmp;

	if (!*b)
		return (0);
	tmp = *b;
	*b = (*b)->next;
	tmp->next = *a;
	*a = tmp;
	if (g_o.silence == 0)
		ft_write_name("pa", 2);
	return (1);
}

int		ft_pb(t_swap **a, t_swap **b)
{
	t_swap		*tmp;

	if (!*a)
		return (0);
	tmp = *a;
	*a = (*a)->next;
	tmp->next = *b;
	*b = tmp;
	if (g_o.silence == 0)
		ft_write_name("pb", 2);
	return (1);
}
