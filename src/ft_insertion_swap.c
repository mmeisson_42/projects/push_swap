/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_insertion_swap.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:04:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 10:01:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static size_t		ft_search(t_swap **a, t_swap **b, size_t *i)
{
	int		score;

	score = 0;
	score += ft_ra(a, b);
	score += ft_pb(a, b);
	score += ft_rra(a, b);
	while (*i > 0 && (*a)->nbr > (*b)->nbr)
	{
		score += ft_rra(a, b);
		(*i)--;
	}
	return (score);
}

int					ft_insertion_swap(t_swap **a, size_t len)
{
	int		score;
	size_t	i;
	t_swap	*b;

	i = 0;
	score = 0;
	b = NULL;
	while (i < len - 1)
	{
		while ((*a)->nbr > (*a)->next->nbr)
		{
			score += ft_search(a, &b, &i);
			score += ft_pa(a, &b);
			if ((*a)->nbr > (*a)->next->nbr)
				score += ft_sa(a, &b);
		}
		score += ft_ra(a, &b);
		i++;
		ft_printstate(*a, b);
	}
	score += ft_ra(a, &b);
	return (score);
}
