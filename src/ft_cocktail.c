#include "push.h"

int		ft_is_sort(t_swap *a, size_t size)
{
	size_t		i;

	i = 0;
	while (i < size - 1)
	{
		if (a->nbr > a->next->nbr)
			return (0);
		a = a->next;
		i++;
	}
	return (1);
}

int		ft_cocktail(t_swap **a, size_t size)
{
	size_t		i;
	t_swap		*b;
	int			score;

	i = 0;
	b = NULL;
	score = 0;
	while (42)
	{
		while (i < size - 2)
		{
			ft_printf("first\n");
			if ((*a)->nbr > (*a)->next->nbr)
				score += ft_sa(a, &b);
			score += ft_ra(a, &b);
			i++;
		}
		ft_swapprint(*a, 10);
		if (ft_is_sort(*a, size) == 1)
			break ;
		while (i > 0)
		{
			ft_printf("second\n");
			if ((*a)->nbr > (*a)->next->nbr)
				score += ft_sa(a, &b);
			score += ft_rra(a, &b);
			i--;
		}
		ft_swapprint(*a, 10);
		if (ft_is_sort(*a, size) == 1)
			break ;
	}
	while (i++ < size)
		score += ft_ra(a, &b);
	return (score);
}
