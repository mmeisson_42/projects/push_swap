/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 10:33:55 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 15:50:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static void		ft_bornes(char *nbr)
{
	size_t		i;

	i = 0;
	while (nbr[i] == ' ' || nbr[i] == '\t')
		i++;
	if (nbr[i] == '-')
	{
		if ((ft_strlen(nbr + i + 1) == 10 &&
				ft_strcmp("2147483648", nbr + i + 1) < 0)
					|| ft_strlen(nbr + i + 1) > 10)
			ft_error();
	}
	else
	{
		if (nbr[i] == '+')
			i++;
		if ((ft_strlen(nbr + i) == 10 &&
				ft_strcmp("2147483647", nbr + i) < 0)
					|| ft_strlen(nbr + i) > 10)
			ft_error();
	}
}

int				ft_check_error(char *nbr, t_swap_d *d)
{
	int			n;
	t_swap		*a;

	ft_bornes(nbr);
	a = d->swap;
	n = ft_atoi(nbr);
	while (a)
	{
		if (n == a->nbr)
			ft_error();
		a = a->next;
	}
	return (n);
}
