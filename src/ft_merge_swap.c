/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_swap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:04:45 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 02:55:24 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static int		ft_merge(t_swap **a, t_swap **b, size_t len_a)
{
	int			score;
	size_t		i;

	score = 0;
	i = 0;
	while (i < len_a)
	{
		while (*b && (*b)->nbr < (*a)->nbr)
		{
			score += ft_pa(a, b);
			score += ft_ra(a, b);
		}
		score += ft_ra(a, b);
		i++;
	}
	while (*b)
	{
		score += ft_pa(a, b);
		score += ft_ra(a, b);
	}
	return (score);
}

int				ft_merge_swap(t_swap **a, size_t len)
{
	t_swap		*b;
	int			score;
	size_t		i;

	b = NULL;
	i = 0;
	score = 0;
	if (len < 2)
		return (0);
	else if (len == 2)
	{
		if ((*a)->nbr > (*a)->next->nbr)
			return (ft_sa(a, &b));
		return (0);
	}
	i = len / 2;
	while (i--)
		score += ft_pb(a, &b);
	score += ft_merge_swap(a, len - (len / 2));
	score += ft_merge_swap(&b, len / 2);
	ft_printstate(*a, b);
	score += ft_merge(a, &b, len - (len / 2));
	return (score);
}
