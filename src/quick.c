#include "push.h"
# include <time.h>
# include <stdio.h>


void	swap_int(int *a, int *b)
{
	int		c;

	c = *a;
	*a = *b;
	*b = c;
}

void	sort(int *tab, size_t size)
{
	size_t		j;
	int			tem;

	tem = 1;
	while (tem == 1)
	{
		j = 1;
		tem = 0;
		while (j < size)
		{
			if (tab[j - 1] > tab[j])
			{
				swap_int(&tab[j - 1], &tab[j]);
				tem = 1;
			}
			j++;
		}
		size--;
	}
}

int		mediane(t_swap *a, size_t size)
{
	int		tab[size];
	size_t	i;

	i = 0;
	while (i < size)
	{
		tab[i] = a->nbr;
		i++;
		a = a->next;
	}
	sort(tab, size);
	return (tab[size / 2]);
}

void	stack_push(t_stack **stack, size_t val)
{
	t_stack		*new;

	if (!(new = malloc(sizeof(*new))))
		exit(write(2, "Error\n", 6));
	new->nbr = val;
	new->next = *stack;
	*stack = new;
}

t_stack	*stack_pop(t_stack **stack)
{
	t_stack		*tmp;

	tmp = NULL;
	if (*stack)
	{
		tmp = *stack;
		*stack = tmp->next;
	}
	return (tmp);
}

int		quick_exec(t_swap **a, t_stack *s)
{
	t_swap	*b;
	int		score;
	int		pivot;
	int		i;
	int		j;

	i = 0;
	j = 0;
	score = 0;
	b = NULL;
	pivot = mediane(*a, s->nbr);
	while (i < s->nbr)
	{
		if ((*a)->nbr < pivot)
			score += ft_pb(a, &b);
		else
		{
			score += ft_ra(a, &b);
			j++;
		}
		i++;
	}
	while (j--)
		score += ft_rra(a, &b);
	while (b)
		score += ft_pa(a, &b);
	return (score);
}

int		quick(t_swap **a, size_t len)
{
	t_stack		*stack;
	t_stack		*this;
	int			score = 0;
	int			i;

	stack = NULL;
	stack_push(&stack, len);
	while (stack)
	{
		this = stack_pop(&stack);
		if (this->nbr > 1)
		{
			stack_push(&stack, this->nbr - (this->nbr / 2));
			stack_push(&stack, this->nbr / 2);
		}
		i = 0;
		if (this->nbr > 2)
			score += quick_exec(a, this);
		else if (this->nbr > 1)
		{
			if ((*a)->nbr > (*a)->next->nbr)
				score += ft_sa(a, NULL);
		}
		else if (this->nbr == 1)
			score += ft_ra(a, NULL);
		free(this);
	}
	return (score);
}
