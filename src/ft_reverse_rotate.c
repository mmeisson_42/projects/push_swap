/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse_rotate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:03:06 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:39:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

extern t_opt	g_o;

int		ft_rra(t_swap **a, t_swap **b)
{
	t_swap		*prev;
	t_swap		*tmp;

	if (b)
		b = NULL;
	if (!*a || !(*a)->next)
		return (0);
	tmp = *a;
	prev = NULL;
	while (tmp->next)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	prev->next = NULL;
	tmp->next = *a;
	*a = tmp;
	if (g_o.silence == 0)
		ft_write_name("rra", 3);
	return (1);
}

int		ft_rrb(t_swap **a, t_swap **b)
{
	t_swap		*prev;
	t_swap		*tmp;

	if (a)
		a = NULL;
	if (!*b || !(*b)->next)
		return (0);
	tmp = *b;
	prev = NULL;
	while (tmp->next)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	prev->next = NULL;
	tmp->next = *b;
	*b = tmp;
	if (g_o.silence == 0)
		ft_write_name("rrb", 3);
	return (1);
}

int		ft_rrr(t_swap **a, t_swap **b)
{
	int		ret;

	ret = 0;
	ret = ft_rra(a, b);
	ret += ft_rrb(a, b);
	if (ret > 0 && g_o.silence == 0)
		ft_write_name("rrr", 3);
	return (ret) ? 1 : 0;
}
