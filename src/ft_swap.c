/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:04:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 06:40:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

extern t_opt	g_o;

int		ft_sa(t_swap **a, t_swap **b)
{
	int		tmp;

	if (b)
		b = NULL;
	if (!*a || !(*a)->next)
		return (0);
	tmp = (*a)->nbr;
	(*a)->nbr = (*a)->next->nbr;
	(*a)->next->nbr = tmp;
	if (g_o.silence == 0)
		ft_write_name("sa", 2);
	return (1);
}

int		ft_sb(t_swap **a, t_swap **b)
{
	int		tmp;

	if (*a)
		;
	if (!*b || !(*b)->next)
		return (0);
	tmp = (*b)->nbr;
	(*b)->nbr = (*b)->next->nbr;
	(*b)->next->nbr = tmp;
	if (g_o.silence == 0)
		ft_write_name("sb", 2);
	return (1);
}

int		ft_ss(t_swap **a, t_swap **b)
{
	int		ret;

	ret = ft_sa(a, b);
	ret += ft_sb(a, b);
	if (ret > 0 && g_o.silence == 0)
		ft_write_name("ss", 2);
	return (ret) ? 1 : 0;
}
