/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_quick_swap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 15:04:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/29 09:17:15 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push.h"

static int		ft_quick_merge(t_swap **a, t_swap **b)
{
	int		score;

	score = 0;
	while (*b)
	{
		score += ft_pa(a, b);
		score += ft_ra(a, b);
	}
	return (score);
}

static int		ft_devide(t_swap **a, t_swap **b, size_t len, size_t *b_len)
{
	int		score;
	int		pivot;
	size_t	i;

	i = 0;
	score = 0;
	pivot = (*a)->nbr;
	while (i < len)
	{
		if ((*a)->nbr > pivot)
		{
			score += ft_pb(a, b);
			(*b_len)++;
		}
		else
			score += ft_ra(a, b);
		i++;
	}
	score += ft_ra(a, b);
	return (score);
}

int				ft_quick_swap(t_swap **a, size_t len)
{
	t_swap	*b;
	size_t	b_len;
	int		score;

	if (len < 2)
		return (0);
	else if (len < 13)
		return (ft_insertion_swap(a, len));
	b = NULL;
	score = 0;
	b_len = 0;
	score += ft_devide(a, &b, len, &b_len);
	score += ft_quick_swap(a, len - b_len);
	score += ft_quick_swap(&b, b_len);
	score += ft_quick_merge(a, &b);
	ft_printstate(*a, b);
	return (score);
}
