#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

typedef struct tree
{
	struct tree		*left;
	struct tree		*right;
	int				nbr;
}	tree;

tree		*new(int nbr)
{
	tree	*node = malloc(sizeof(*node));

	if (node == NULL)
		exit(1);
	node->nbr = nbr;
	node->left = NULL;
	node->right = NULL;
	return node;
}

void		add(tree **begin, tree *node)
{
	tree	*tmp;

	if (*begin == NULL) {
		*begin = node;
	} else {
		tmp = *begin;
		while (1) {
			if (node->nbr < tmp->nbr) {
				if (tmp->left) {
					tmp = tmp->left;
				} else {
					tmp->left = node;
					return ;
				}
			} else {
				if (tmp->right) {
					tmp = tmp->right;
				} else {
					tmp->right = node;
					return ;
				}
			}
		}
	}
}

int			search(tree *begin, int nbr)
{
	while (begin) {
		if (begin->nbr == nbr) {
			return (1);
		} else if (begin->nbr > nbr) {
			begin = begin->left;
		} else {
			begin = begin->right;
		}
	}
	return (0);
}

int		main(int ac, char **av)
{
	int		occ = 500;
	int		nbr;
	tree	*node = NULL;

	srand(time(NULL));
	if (ac == 2) {
		occ = atoi(av[1]);
	}
	for (int i = 0; i < occ; i++) {
		do {
			nbr = rand() % (occ * 5);
			if (rand() % 2 == 0)
				nbr = -nbr;
		} while (search(node, nbr) == 1);
		add(&node, new(nbr));
		printf("%d ", nbr);
	}
	printf("\n");
	return (0);
}
