#!/bin/bash

exe="./push_swap"

echo " ## Tests de serie de 3"
$exe -1 0 1
$exe -1 1 0
$exe 0 1 -1
$exe 0 -1 1
$exe 1 0 -1
$exe 1 -1 0

echo "\n ## Test de nombre trie"
$exe 1 2 3 4 5 6 7 8 9
$exe -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9
$exe 1 3 5 7 9 11 13 15 17 19
$exe -19 -17 -15 -13 -12 -11 -9 -7 -5 -4 -3 -1 1 3 5 7 9 11 13 15 17 19

echo "\n ## Test avec la premiere sequence non triee"
$exe 2 1 3 4 5 6 7 8 9
$exe -8 -9 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9
$exe 3 1 5 7 9 11 13 15 17 19
$exe -17 -19 -15 -13 -12 -11 -9 -7 -5 -4 -3 -1 1 3 5 7 9 11 13 15 17 19

echo "\n ## Test avec la derniere sequence non triee "
$exe 1 2 3 4 5 6 7 9 8
$exe -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 9 8
$exe 1 3 5 7 9 11 13 15 19 17
$exe -19 -17 -15 -13 -12 -11 -9 -7 -5 -4 -3 -1 1 3 5 7 9 11 13 15 19 17
