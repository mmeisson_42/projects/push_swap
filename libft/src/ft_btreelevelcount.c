/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreelevelcount.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 14:42:59 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/23 20:18:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_btreelevelcount(const t_btree *btree)
{
	size_t	l;
	size_t	r;

	if (!btree)
		return (0);
	l = ft_btreelevelcount(btree->left);
	r = ft_btreelevelcount(btree->right);
	return (l > r) ? (l + 1) : (r + 1);
}
