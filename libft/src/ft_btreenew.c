/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreenew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:34:43 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/27 18:21:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_btree		*ft_btreenew(const void *content, size_t content_size)
{
	t_btree		*n_node;
	void		*copy;

	if (!(n_node = ft_memalloc(sizeof(t_btree))))
		return (NULL);
	if (content)
	{
		if (!(copy = malloc(content_size)))
			return (NULL);
		ft_memcpy(copy, content, content_size);
		n_node->content = copy;
		n_node->content_size = content_size;
	}
	return (n_node);
}
